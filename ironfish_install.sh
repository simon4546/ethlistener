#!/bin/bash
# curl -fsSL https://gitee.com/simon4546/ethlistener/raw/master/ironfish_install.sh|sudo bash 
set -euo pipefail
## sudo apt install curl
curl -fsSL https://get.docker.com |bash -s docker --mirror Aliyun
curl -L -o /usr/local/bin/docker-compose  https://get.daocloud.io/docker/compose/releases/download/1.28.5/docker-compose-`uname -s`-`uname -m`
chmod +x /usr/local/bin/docker-compose 
mkdir -p /etc/docker/
echo '{"registry-mirrors":["https://reg-mirror.qiniu.com/","https://t3yqbami.mirror.aliyuncs.com"]}' | tee -a /etc/docker/daemon.json > /dev/null
systemctl daemon-reload
systemctl restart docker

docker run --rm -i --network host --volume $HOME/.ironfish:/root/.ironfish ghcr.io/iron-fish/ironfish:latest config:set blockGraffiti "planter"
cpu_count=$(grep -c '^processor' /proc/cpuinfo)
# if (( cpu_count > 32 )); then
if [ "$cpu_count" -gt 32 ]; then
tee ./docker-compose.yml << END
version: "3"
services:
    ironfish:
        image: ghcr.io/iron-fish/ironfish:latest
        network_mode: host
        container_name: ironfish
        volumes:
            - ${HOME}/.ironfish:/root/.ironfish
    miner:
        image: ghcr.io/iron-fish/ironfish:latest
        network_mode: host
        container_name: miner
        volumes:
            - ${HOME}/.ironfish:/root/.ironfish
        entrypoint: ["./bin/run", "miners:start", "--threads", "-1"] 
        depends_on:
            - ironfish
    miner2:
        image: ghcr.io/iron-fish/ironfish:latest
        network_mode: host
        container_name: miner2
        volumes:
            - ${HOME}/.ironfish:/root/.ironfish
        entrypoint: ["./bin/run", "miners:start", "--threads", "-1"] 
        depends_on:
            - ironfish
END
else
tee ./docker-compose.yml << END
version: "3"
services:
    ironfish:
        image: ghcr.io/iron-fish/ironfish:latest
        network_mode: host
        container_name: ironfish
        volumes:
            - ${HOME}/.ironfish:/root/.ironfish
    miner:
        image: ghcr.io/iron-fish/ironfish:latest
        network_mode: host
        container_name: miner
        volumes:
            - ${HOME}/.ironfish:/root/.ironfish
        entrypoint: ["./bin/run", "miners:start", "--threads", "-1"] 
        depends_on:
            - ironfish
END
fi

docker-compose pull && docker-compose up -d