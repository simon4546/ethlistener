from decimal import Decimal
from bson.decimal128 import Decimal128
import json
import json
import urllib.request
import os
import emails
import threading
ERC20_ABI = json.loads('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]')
ether_scan_api = '4FCSY37NY3D4UHNIC74157IGR16J9HPE5S'

# for log in receipt.logs:
#     if log["topics"][0]==transfer_event_hashed:
#         contract_address = w3.eth.web3.toChecksumAddress(log["address"])
#         token_contract = w3.eth.contract(address=contract_address ,abi=get_abi(contract_address))
#         symbol = str(token_contract.functions.symbol().call()[:3])
#         print(symbol)



def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

def send_mail(html: str, subject: str, to: str):
    message = emails.html(html=html, subject=subject, mail_from=(
        os.getenv('mailuser'), os.getenv('mailfrom')))
    r = message.send(to=to, smtp={'host': os.getenv('mailhost'), 'timeout': 5,
                     'port': 25,  'user': os.getenv('mailfrom'), 'password': os.getenv("mailpass")})


def convert_decimal_for_dict(dict_item):
    # This function iterates a dictionary looking for types of Decimal and converts them to Decimal128
    # Embedded dictionaries and lists are called recursively.
    if dict_item is None:
        return None

    for k, v in list(dict_item.items()):
        if isinstance(v, dict):
            convert_decimal(v)
        elif isinstance(v, list):
            for l in v:
                convert_decimal(l)
        elif isinstance(v, Decimal):
            dict_item[k] = Decimal128(str(v))

    return dict_item


def get_abi(erc_20_address: str):
    requrl = f'https://api.etherscan.io/api?module=contract&action=getabi&address={erc_20_address}&apikey={ether_scan_api}'
    while True:
        with urllib.request.urlopen(requrl) as url:
            if url.code != 200:
                raise NotImplementedError(
                    'I don\'t know how to handle HTTP code {} :('.format(url.code))
            else:
                result = json.loads(url.read())

                if result['status'] == '1':
                    break
    return json.loads(result['result'])


def convert_decimal(v):
    if isinstance(v, Decimal):
        return Decimal128(str(v))
    return None

def exception_handler(func):
    def inner_function(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as err:
            print(f"{func.__name__} {err}")
    return inner_function